
import 'dart:convert';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:sflober_p1/Models/productos_model.dart';
import 'package:sflober_p1/views/product_details_view.dart';
import 'package:sflober_p1/Models/productos_data.dart';


class List_page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Productos"),
        backgroundColor: Colors.deepPurple,
      ),
      body: Center(
        child: FutureBuilder(
          future: Future.delayed(const Duration(seconds: 2), (){
            return DefaultAssetBundle.of(context).loadString("assets/json/products.json");
          } ),
          builder: (context, snapshot) {
            if(snapshot.hasData) {
              Map<String, dynamic> data= json.decode(snapshot.data.toString());
              var products = Products.fromJson(data);
              return MyList(context, products);
            } else{
              return Center(child: LinearProgressIndicator(),);
            }
          },
        ),
      ),
    );
  }
  Widget MyList(BuildContext context, Products products){
    var bookList = products.products.map((product) =>
        GestureDetector(
            child: Hero(
              tag: product.name,
              flightShuttleBuilder: (BuildContext flightContext,
                  Animation<double> animation,
                  HeroFlightDirection flightDirection,
                  BuildContext fromHeroContext,
                  BuildContext toHeroContext) => Material(child: toHeroContext.widget),
              child:  ListTile(
                leading: ConstrainedBox(constraints:const BoxConstraints(
                    minWidth: 50,
                    minHeight: 50,
                    maxHeight: 50,
                    maxWidth: 50
                ),
                  child: Image.asset(product.image, fit:BoxFit.cover),),
                  title: Text("${product.name}"),
                  subtitle: Text("${product.price} €") ,
                  trailing: Icon(Icons.keyboard_arrow_right),
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context)=>ProductDetailsPage(data: product,),
                    ));
                  },
              ),
            )
        )
    
).toList();



    return ListView.separated(
        itemBuilder: (context, index) => bookList[index],
        itemCount: bookList.length,
        separatorBuilder: (context, index) {
          switch(Books.initial[index].types){
            case BooksTypes.fantasia:
              return Divider(color: Color.fromARGB(255, 235, 174, 9), thickness: 2,);
            case BooksTypes.scifi:
              return Divider(color: Color.fromARGB(255, 3, 91, 163),thickness: 2,);
          }

        });

  }
}

