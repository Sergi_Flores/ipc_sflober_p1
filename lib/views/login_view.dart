import 'package:flutter/material.dart';
import 'package:sflober_p1/views/products_list_view.dart';



class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row( 
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(15),
                  color: Colors.white,
                  child: const Text("Login",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 30, color: Colors.black,
                        fontWeight: FontWeight.bold, fontFamily: "Helvetica"
                    ),
                  ),
                ),
                Spacer(),
                Container(
                  width: 75,
                  height: 50,
                  decoration:
                  const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(4, 4),
                          blurRadius: 4,
                        )
                      ],
                      color: Colors.deepPurple,
                      shape: BoxShape.circle
                  ),
                ),
                Container(
                  width: 100,
                  height: 150,
                  decoration:
                  const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(4, 4),
                          blurRadius: 4,
                        )
                      ],
                      color: Colors.deepPurple,
                      shape: BoxShape.circle
                  ),
                ),
                Container(
                  width: 25,
                  height: 25,
                  decoration:
                  const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(4, 4),
                          blurRadius: 4,
                        )
                      ],
                      color: Colors.deepPurple,
                      shape: BoxShape.circle
                  ),
                ),
              ],
            ),
            const TextField(
              decoration: InputDecoration(
                icon: Icon(Icons.account_circle_rounded, color: Colors.deepPurple,),
                hintText: 'Username',
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                  icon: Icon(Icons.vpn_key, color: Colors.deepPurple),
                  hintText: 'Password'
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.deepPurple,
                    border: Border.all(
                      color: Colors.deepPurple,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    boxShadow:  const [
                      BoxShadow(
                          color: Colors.black45,
                          offset: Offset(4,4),
                          blurRadius: 4,
                      ),
                    ],
                  ),
                  child: GestureDetector(
                     onTap: (){
                       var route = MaterialPageRoute(
                         builder: (context)=> List_page(),
                       );
                       Navigator.of(context).push(route);
                     },
                     child: const Text("Login",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20, color: Colors.white,
                            fontWeight: FontWeight.bold, fontFamily: "Helvetica"
                        ),
                      ),
                    ),
                  padding: const EdgeInsets.all(12),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
