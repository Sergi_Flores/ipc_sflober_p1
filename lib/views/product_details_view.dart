
import 'package:flutter/material.dart';
import 'package:sflober_p1/Models/productos_data.dart';

class ProductDetailsPage extends StatelessWidget {
  final Product data;

  ProductDetailsPage({required this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detalles del producto "),
        backgroundColor: Colors.deepPurple,
      ),
      body: Column(
        children: [
          Hero(
              tag: data.name,
              child: Container(
               margin: const EdgeInsets.symmetric(horizontal: 80, vertical: 20),
               height: 200,
               width: 300,
               child: Image.asset(data.image),
               decoration: BoxDecoration(
                 color: Colors.white,
                 boxShadow: [
                   BoxShadow(
                     color: Colors.grey.withOpacity(0.5),
                     blurRadius: 10,
                     spreadRadius: 10)
                 ],
                 borderRadius: BorderRadius.circular(10)),
               ),
          ),
          _Productos(data: this.data,),
        ],
      ),
    );
  }
}

class _Productos extends StatelessWidget {
  final Product data;
   _Productos({required this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 399,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30),
          topLeft: Radius.circular(30),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 25, vertical:10),
            child: Text("${data.name}",style: const TextStyle(fontSize: 30, fontWeight:FontWeight.bold),),
          ),
          Container(
          margin: const EdgeInsets.symmetric(horizontal: 25, vertical:5),
          child: Text("${data.price} €",style: const TextStyle(fontSize: 15,),),
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 10,
                      spreadRadius: 10)
                ],
                borderRadius: BorderRadius.circular(10)),
            margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
            child: Text("${data.description}",style: const TextStyle(fontSize: 20,),),
          ),
        ],
      ),
    );
  }
}
