import 'package:flutter/material.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
      body: Container(
        color: Colors.blue,
        child: const Text('Hello, Flutter!'),
      ),
        
      );
}