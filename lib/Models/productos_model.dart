class Books {
  late String name;
  late double price;
  late String category;
  late BooksTypes  types;
  late String? iconPath;

  static final List<Books> initial = [
    Books( name: "Juego de tronos", price: 19.95, category: "Fantasía", types: BooksTypes.fantasia, iconPath: "assets/img/got.jpg" ),
    Books( name: "Los magos", price: 16.95, category: "Fantasía", types: BooksTypes.fantasia, iconPath: "assets/img/magos.jpg" ),
    Books( name: "Harry potter", price: 18.95, category: "Fantasía", types: BooksTypes.fantasia, iconPath: "assets/img/Harry.jpg" ),
    Books( name: "1984", price: 24.95, category: "Ciencia ficción", types: BooksTypes.scifi, iconPath: "assets/img/1984.jpg" ),
    Books( name: "El hobbit", price: 29.95, category: "Fantasía", types: BooksTypes.fantasia, iconPath: "assets/img/Hobbit.jpg" ),
    Books( name: "El corredor del laberinto", price: 18.95, category: "Ciencia ficción", types: BooksTypes.scifi, iconPath: "assets/img/Corredor.jpg" ),
    Books( name: "Tokyo ghoul", price: 7.95, category: "Ciencia ficción", types: BooksTypes.scifi, iconPath: "assets/img/Tokyo.jpg" ),
  ];

  Books({
    required this.name,
    required this.price,
    required this.category,
    required this.types,
    this.iconPath});

}
enum BooksTypes {fantasia,scifi}

